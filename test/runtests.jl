using SuspScreen
using Test
using CSV
using DataFrames
using Pkg 


try 
    using MS_Import
    #using Cent2Profile
catch
    @warn("MS_Import is being installed")
    Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))
    #Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/cent2profile.jl/src/master/"))
    using MS_Import
    #using Cent2Profile
end




@testset "SuspScreen.jl" begin

    path2sus = "Test_susList.csv"
    mode = "POSITIVE"
    source = "ESI"
    

    # File import
    pathin=""
    filenames=["TestChrom.mzXML"]
    mz_thresh = [100,400]
    int_thresh = 500  
    
    chrom=import_files(pathin,filenames,mz_thresh,int_thresh)


    # Converting the enrties 

    sus_e = suslist2entries(path2sus,mode,source);
    #println(sus_e)

    # Suspect Screening 
    mass_tol = 0.05
    min_int = 2000
    iso_depth = 5
    rt_width = 0.5

    table = suspect_screening(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)
    println(table)
    @test table[!,"NrMatchedFrags"][1] == 5
    @test table[!,"NrTestedFrags"][1] == 35
    @test table[!,"MatchFactor"][3] == 0.62


end
