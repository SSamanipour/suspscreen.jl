using SuspScreen

using CSV 
using DataFrames
using MS_Import
using BenchmarkTools



##########################################################

path2sus = "/Path/to/SusList.csv"
mode = "POSITIVE"
source = "ESI"


# Converting the enrties 

sus_e = suslist2entries(path2sus,mode,source);

# File import
pathin="/path/to/xzXML/files"

mz_thresh = [100,400]
int_thresh = 1000  
    


# Suspect Screening 
mass_tol = 0.05
min_int = 2000
iso_depth = 5
rt_width = 0.5



## 

function batch_susp_screen(pathin,sus_e,mz_thresh,int_thresh,mass_tol,min_int,iso_depth,rt_width)
    
    file_name = readdir(pathin)

    for i=1:size(file_name,1)
        m = split(file_name[i,1],".")
        if m[1] == 0 || m[end] != "mzXML"
            continue
        end 
        filenames = [file_name[i,1]]
        chrom=import_files(pathin,filenames,mz_thresh,int_thresh)
        table = suspect_screening(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)

        output=string(pathin,"/",m[1],"_SusScreenReport.csv")
        CSV.write(output,table)

    end 


end 




@time batch_susp_screen(pathin,sus_e,mz_thresh,int_thresh,mass_tol,min_int,iso_depth,rt_width)

