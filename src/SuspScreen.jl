module SuspScreen

using DataFrames
using CSV
#using Plots
using LinearAlgebra
using Statistics
using BenchmarkTools
using ProgressBars

# Write your package code here.

include("PeakFind.jl")
include("Screening.jl")

export suslist2entries, proteine_screening_ms1, suspect_screening

end
