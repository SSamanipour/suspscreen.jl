# SuspScreen

[![Build Status](https://ci.appveyor.com/api/projects/status/github/saersamani/SuspScreen.jl?svg=true)](https://ci.appveyor.com/project/saersamani/SuspScreen-jl)


**SuspScreen.jl** is a julia package for performing suspect screening of XC-HRMS data using a list of Chemicals of Emerging Concern.

## Installation

Given that **SuspScreen.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/suspscreen.jl/src/master/"))


```
